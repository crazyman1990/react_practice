export interface Project {
    id: number;
    project_name: string;
    project_desc: string;
    startDate?: Date;
    endDate?: Date;
}