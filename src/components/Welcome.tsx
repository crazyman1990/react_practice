import * as React from 'react';

export const Welcome = (props) => (<p>Welcome {props.username}</p>);
