import * as React from 'react';
import { Table } from 'react-bootstrap';
import { Project } from './Project';
import { Project as model } from '../models/Project';

export const ProjectList = (props: { data: Array<model>}) => {
    const list = props.data.map((p) => <Project key={p.id} project={p}/>);
    return (
    <Table responsive>
        <thead>
            <tr>
                <th>ID</th>
                <th>Project name</th>
                <th>Project description</th>
                <th>Start date</th>
                <th>End date</th>
            </tr>
        </thead>
        <tbody>
            {list}
        </tbody>
    </Table>
    );
};