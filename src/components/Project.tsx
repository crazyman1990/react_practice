import * as React from 'react';
import { Project as model } from '../models/Project';

interface ProjectProps {
    project: model
}

export const Project = (props: ProjectProps) => (
    <tr>
        <td>{props.project.id}</td>
        <td>{props.project.project_name}</td>
        <td>{props.project.project_desc}</td>
        <td>{props.project.startDate.toDateString()}</td>
        <td>{props.project.endDate && props.project.endDate.toDateString()}</td>
    </tr>
);