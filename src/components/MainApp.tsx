import * as React from 'react';
import { HeaderBar } from './HeaderBar';
import {MainPanel} from './MainPanel';

export const MainApp = (props) => (
    <div>
    <HeaderBar username={props.username}/>
    <MainPanel/>
    </div>
);