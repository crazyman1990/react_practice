import * as React from 'react';
import {Form, FormGroup, FormControl, ControlLabel, HelpBlock, ButtonToolbar, Button} from 'react-bootstrap';
import { Field } from './Field';

interface LoginProps {
    LoginEvent: Function
}

interface LoginState {
    username: string,
    password: string,
    isError: boolean
}

export class Login extends React.Component<LoginProps, LoginState> {
    isUsernameModified: boolean = false;
    isPasswordModified: boolean = false;
    isError: boolean = false;

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            isError: false
        }
        //this.validatePassword.call(this);
        //this.validateUsername.call(this);
    }

    onUsernameChange = (e: React.FormEvent<HTMLInputElement>): void => {
        this.isUsernameModified = true;
        this.setState({ username: e.currentTarget.value });
    }

    onPasswordChange = (e: React.FormEvent<HTMLInputElement>): void => {
        this.isPasswordModified = true;
        this.setState({ password: e.currentTarget.value });
    }

    private validateUsername = (): string => {
        if (this.isUsernameModified && this.state.username == '') {
            return "error";
        }
        return "success";
    }

    private validatePassword = (): string => {
        if (this.isPasswordModified && this.state.username == '') {
            return "error";
        }
        return "success";
    }

    login = (e: React.FormEvent<Button>): void => {
        if (this.state.username.toLocaleLowerCase() == 'tinh' && this.state.password == '123456') {
            this.setState({
                isError: false
            })
            this.props.LoginEvent(this.state.username);
        } else {
            this.setState({
                isError: true
            })
        }
        console.log('login');
    }

    render() {
        return (
        <Form>
            <Field 
                id="username" 
                label="Username" 
                placeholder="Username" 
                type="text" 
                value={this.state.username} 
                handleChangeEvent={this.onUsernameChange} 
                validateInput={this.validateUsername}/>

            <Field 
                id="password" 
                label="Password" 
                placeholder="Password" 
                type="password" 
                value={this.state.password} 
                handleChangeEvent={this.onPasswordChange} 
                validateInput={this.validatePassword}/>
            {this.state.isError && <p className="bg-danger">Wrong username or password.</p>}
            <ButtonToolbar>
                <Button disabled={this.state.username.length == 0 || this.state.password.length == 0} onClick={this.login}>Login</Button>
            </ButtonToolbar>
        </Form>);
    };
}