import * as React from 'react';
import { ListGroup, ListGroupItem } from 'react-bootstrap';

interface MenuPanelProps {
    showEmployees: React.EventHandler<any>;
    showProjects: React.EventHandler<any>;
}

export const MenuPanel = (props: MenuPanelProps) => (
    <ListGroup>
        <ListGroupItem active onClick={props.showProjects}>Project</ListGroupItem>
        <ListGroupItem onClick={props.showEmployees}>Employee</ListGroupItem>
    </ListGroup>
)