import * as React from 'react';
import { FormGroup, ControlLabel, FormControl, HelpBlock } from 'react-bootstrap';

interface FieldProps {
    id: string,
    label: string,
    type: string,
    placeholder: string,
    value: string,
    handleChangeEvent: Function,
    validateInput: Function
}

export const Field = (props: FieldProps) => (
    <FormGroup controlId={props.id} validationState={props.validateInput()}>
        <ControlLabel>{props.label}</ControlLabel>
        <FormControl 
            type={props.type} 
            placeholder={props.placeholder} 
            value={props.value} 
            onChange={(e) => props.handleChangeEvent(e)}/>
    </FormGroup>
)