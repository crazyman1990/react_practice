import * as React from 'react';
//import { BrowserRouter, Link, Route, Switch } from 'react-router-dom';

//import {LinkContainer} from 'react-router-bootstrap';
import { Login } from './Login';
import { MainApp } from './MainApp'


interface AppState {
    userLogin: string;
    isLogin: boolean
}

export class App extends React.Component<{}, AppState> {
    constructor(props) {
        super(props);
        this.state = {
            userLogin: '',
            isLogin: false
        }
    }

    onLoginSuccess = (username): void => {
        this.setState({
            userLogin: username,
            isLogin: true
        });
    }
    render() {
        const isLogin = this.state.isLogin;

        return (
            <div className="container">
                {!isLogin ? <Login LoginEvent={this.onLoginSuccess}/> : <MainApp username={this.state.userLogin}/>}
            </div>
        )
    }
}