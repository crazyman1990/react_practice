import * as React from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';
import { Welcome } from './Welcome';

export const HeaderBar = (props) => (
    <Navbar>
        <Navbar.Header>
            <Navbar.Brand>
                Project Management
            </Navbar.Brand>
        </Navbar.Header>
        <Nav pullRight>
            <Welcome username={"tinh"}/>
        </Nav>
    </Navbar>
)