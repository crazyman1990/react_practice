import * as React from 'react';
import {MenuPanel} from './MenuPanel';
import {Col} from 'react-bootstrap';
import {ProjectList} from './ProjectList';
import {Employee} from './Employee';
import {Project as projectModel} from '../models/Project';
const DisplayPanel = {
    ProjectPanel: ProjectList,
    EmployeePanel: Employee
};

interface MainPanelStates {
    openPanel: string;
    projects: Array<projectModel>;
}

export class MainPanel extends React.Component<{}, MainPanelStates> {

    constructor(props) {
        super(props);
        this.state = {
            openPanel: "ProjectPanel",
            projects: [
                { id: 1, project_name: "product 1", project_desc: "description 1", startDate: new Date() },
                { id: 2, project_name: "product 2", project_desc: "description 1", startDate: new Date() },
                { id: 3, project_name: "product 3", project_desc: "description 1", startDate: new Date() },
                { id: 4, project_name: "product 4", project_desc: "description 1", startDate: new Date() },
                { id: 5, project_name: "product 5", project_desc: "description 1", startDate: new Date() },
            ],
        };
    }

    showProjects = (e) => {
        this.setState({
            openPanel: "ProjectPanel"
        });
    }

    showEmployees = (e) => {
        this.setState({
            openPanel: "EmployeePanel"
        });
    }
    
    render() {
        const Panel = DisplayPanel[this.state.openPanel];
        return (
            <div className="row">
                <Col md={3}>
                    <MenuPanel showProjects={this.showProjects} showEmployees={this.showEmployees}/>
                </Col>
                <Col md={9}>
                    <Panel data={this.state.projects}/>
                </Col>
            </div>
        );
    }
}