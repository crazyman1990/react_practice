import * as React from 'react';
import { Form, FormControl, FormGroup, Button, ControlLabel } from 'react-bootstrap';
import { Project as model } from '../models/Project';

export const ProjectDetail = (props) => {
    let project: model = {
        id: 0,
        project_name: '',
        project_desc: '',
    };

    return (
        <Form>
            <FormGroup>
                <ControlLabel>Project name</ControlLabel>
                <FormControl type="text" placeholder="Project name" value={project.project_name}
                    ref={(input) => {project.project_name = input.props.value.toString();}} />
            </FormGroup>
            <Button onClick={(e) => props.addNewProject(project)}>Add</Button>
        </Form>
    );
};