const path = require("path");
const built_path = path.resolve(__dirname, "www/public");
const src_path = path.resolve(__dirname, "src");

module.exports = {
    entry: {
        main: src_path + "/index.tsx"
    },
    output: {
        path: built_path,
        filename: "bundle.js"
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json']
    },
    module: {
        rules: [
            {test: /\.tsx?$/, loader: "awesome-typescript-loader", exclude: /node_modules/}
        ]
    },
    externals: {
        'react': 'React',
        'react-dom': 'ReactDOM'
    }
}